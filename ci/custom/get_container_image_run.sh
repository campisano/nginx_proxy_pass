#!/usr/bin/env bash

set -o errexit -o nounset -o pipefail

IMAGE="docker.io/nginx:1.25.4-bookworm"

echo "${IMAGE}"
