#!/usr/bin/env bash

set -o errexit -o nounset -o pipefail

if test -f /tmp/build.date
then
    echo -en "Build at "
    cat /tmp/build.date
    echo
fi



rm -f /etc/nginx/conf.d/*.conf



while read SERVER_NAME VALUE
do
    export SERVER_NAME SERVER_PORT
    cat /etc/nginx/conf.d/site.conf.start_template | sed 's/\$/§/g' | sed 's/§{/\${/g' | envsubst | sed 's/§/\$/g' >> "/etc/nginx/conf.d/${SERVER_NAME}.conf"



    ROOT_URL=$(echo "$VALUE" | jq -r '.root // ""')

    if test -n "$ROOT_URL"
    then
        DEST_HOST=$(echo ${ROOT_URL} | cut -d '/' -f -3)
        DEST_PATH=$(echo ${ROOT_URL} | cut -d '/' -f 4-)
        export DEST_HOST DEST_PATH
        cat /etc/nginx/conf.d/site.conf.root_proxy_pass_template | sed 's/\$/§/g' | sed 's/§{/\${/g' | envsubst | sed 's/§/\$/g' >> "/etc/nginx/conf.d/${SERVER_NAME}.conf"
    fi


    PATHS=$(echo "$VALUE" | jq -r '.paths // ""')

    if test -n "$PATHS"
    then
        while read SOURCE_PATH DEST_URL
        do
            DEST_HOST=$(echo ${DEST_URL} | cut -d '/' -f -3)
            DEST_PATH=$(echo ${DEST_URL} | cut -d '/' -f 4-)
            export SOURCE_PATH DEST_HOST DEST_PATH
            cat /etc/nginx/conf.d/site.conf.subpath_proxy_pass_template | sed 's/\$/§/g' | sed 's/§{/\${/g' | envsubst | sed 's/§/\$/g' >> "/etc/nginx/conf.d/${SERVER_NAME}.conf"

        done < <(echo "$PATHS" | jq -r 'to_entries[] | "\(.key)\t\(.value)"')
    fi

    cat /etc/nginx/conf.d/site.conf.end_template | sed 's/\$/§/g' | sed 's/§{/\${/g' | envsubst | sed 's/§/\$/g' >> "/etc/nginx/conf.d/${SERVER_NAME}.conf"
    chmod 644 "/etc/nginx/conf.d/${SERVER_NAME}.conf"

done < <(echo "$PROXY_PASS" | jq -r 'to_entries[] | "\(.key)\t\(.value)"')



rm -f /etc/nginx/conf.d/site.conf.start_template
rm -f /etc/nginx/conf.d/site.conf.root_proxy_pass_template
rm -f /etc/nginx/conf.d/site.conf.subpath_proxy_pass_template
rm -f /etc/nginx/conf.d/site.conf.end_template


exec "$@"
